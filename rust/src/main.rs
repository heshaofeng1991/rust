fn main() {
    let v = vec![1, 2, 3, 4, 5];
    println!("The element of v is {}", v[4]);
//    for i in &v {
//        println!("The element of v is {}", i);
//    }
    let v = vec![0; 10];
    println!("The element of v is {}", v[7]);
//    for i in &v {
//        println!("The element of v is {}", i);
//    }

    let string = "Hello there.";
    println!("{}", string);

    let mut s = "Hello".to_string();
    println!("{}", s);

    s.push_str(", world.");
    println!("{}", s);

    let ss = "Hello".to_string();
    takes_slice(&ss);

    let hachiko = "忠犬ハチ公";
    for b in hachiko.as_bytes() {
        print!("{}, ", b);
    }
    println!("");
    for c in hachiko.chars() {
        print!("{}, ", c);
    }
    println!("");

    let dog = hachiko.chars().nth(1);

    let hachiko2 = "Hello, World !";
    for b in hachiko2.as_bytes() {
        print!("{}, ", b);
    }
    println!("");
    for c in hachiko2.chars() {
        print!("{}, ", c);
    }
    println!("");

    let hello = "Hello, ".to_string();
    let world = "world !";
    let hello_world = hello + world;
    println!("{}", hello_world);

    let hello = "Hello, ".to_string();
    let world = "world !".to_string();
    let hello_world = hello + &world;
    println!("{}", hello_world);
}

fn takes_slice(slice : &str) {
    println!("Got : {}", slice);
}