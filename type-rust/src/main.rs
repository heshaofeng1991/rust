fn main() {
    // String
    println!("Hello, world!");
    println!("Hello, cargo!");
//
//    //int32
//    let x = 42;
//    println!("My lucky number is {}.", x);
//
//    //vector
//    let xs = vec![1, 2, 3];
//    println!("The list is: {:?}", xs);
//    println!("This is information");
//    eprintln!("This is an error! :(");

//         use std::io::{self, Write};
//
//         let stdout = io::stdout(); // get the global stdout entity
//         let mut handle = io::BufWriter::new(stdout); // optional: wrap that handle in a buffer
//         writeln!(handle, "foo: {}", 42); // add `?` if you care about errors here

//         let pb = indicatif::ProgressBar::new(100);
//         for i in 0..100 {
//             //do_hard_work();
//             pb.println(format!("[+] finished #{}", i));
//             pb.inc(1);
//         }
//         pb.finish_with_message("done");

//         use log::{info, warn};
//
//         env_logger::init();
//         info!("starting up");
//         warn!("oops, nothing implemented!");
}
