use std::fmt::Debug;
use std::fmt::Display;

fn foo<T : Clone + Display, K : Clone + Debug>(x : T, y : K) {
    x.clone();
    y.clone();
    println!("{}", x);
    println!("{:?}", y);
    println!("{}, {:?}", x, y);
}

fn bar<T, K>(x : T, y : K)
    where T : Clone + Display,
          K : Clone + Debug {
    x.clone();
    y.clone();
    println!("{}", x);
    println!("{:?}", y);
    println!("{}, {:?}", x, y);
}

trait ConvertTo<Output> {
    fn convert(&self) -> Output;
}

impl ConvertTo<i64> for i32 {
    fn convert(&self) -> i64 { *self as i64 }
}

// can be called with T == i32
fn normal<T: ConvertTo<i64>>(x: &T) -> i64 {
    x.convert()
}

// can be called with T == i64
fn inverse<T>() -> T
// this is using ConvertTo as if it were "ConvertFrom<i32>"
    where i32: ConvertTo<T> {
    1i32.convert()
}

trait Foo {
    fn bar(&self);
    fn baz(&self) { println!("We called baz."); }
}

struct UseDefault;

impl Foo for UseDefault {
    fn bar(&self) { println!("We called bar,."); }
}

struct OverrideDefault;

impl Foo for OverrideDefault {
    fn bar(&self) { println!("We called bar,."); }
    fn baz(&self) { println!("Override baz!"); }
}

trait FooText {
    fn foo_text(&self);
}

trait FooBar : FooText {
    fn foo_bar(&self);
}

struct Baz;

impl FooText for Baz {
    fn foo_text(&self) { println!("foo text"); }
}

impl FooBar for Baz {
    fn foo_bar(&self) { println!("foo bar"); }
}

struct Firework {
    strength : i32,
}

impl Drop for Firework {
    fn drop(&mut self) {
        println!("BOOM times {}!!!", self.strength);
    }
}

fn main() {
    println!("Hello, world!");
    foo("Hello", "world");
    bar("Hello", "world");
    let default = UseDefault;
    default.baz();
    let overRide = OverrideDefault;
    overRide.baz();
    for i in 0..10 {
        let firework = Firework { strength : i };
    }
    let firecracker = Firework { strength : 12 };
    let tnt = Firework { strength : 11 };

    match option {
        Some(x) => {foo_option(x)},
        None => {},
    }

    if option.is_some() {
        let x = option.unwrap();
        foo_option()
    }
}
