use std::thread;

#[no_mangle]
pub extern fn process() -> i32 {
    let handles: Vec<_> = (0..10).map(|_| {
        thread::spawn(|| {
            let mut _x = 0;
            for _ in (0..5_000_001) {
                _x += 1
            }
        })
    }).collect();

    println!("i come in...");

    for h in handles {
        h.join().ok().expect("Could not join a thread!");
    }

    let i = 100;
    println!("{}", i);
    return i
}