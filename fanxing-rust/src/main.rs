enum Option<T> {
    Some(T),
    None,
}

enum Result <T, E> {
    Ok(T),
    Err(E),
}

struct Point<T> {
    x : T,
    y : T,
}

struct Circle {
    x : f64,
    y : f64,
    radius : f64,
}

impl Circle {
    fn area(&self) -> f64 {
        std::f64::consts::PI * (self.radius * self.radius)
    }
}

trait HasArea {
    fn area(&self) -> f64;
}

impl HasArea for Circle {
    fn area(&self) -> f64 {
        std::f64::consts::PI * (self.radius * self.radius)
    }
}

struct Square {
    x : f64,
    y : f64,
    side : f64,
}

impl HasArea for Square {
    fn area(&self) -> f64 {
        self.side.clone() * self.side.clone()
    }
}

impl HasArea for i32 {
    fn area(&self) -> f64 {
        println!("this is silly");
        *self as f64
    }
}

fn print_area<T : HasArea >(shape: T) {
    println!("This shape has an area of {}", shape.area());
}

fn main() {
    println!("Hello, world!");
    // let x : Option<i32> = Some(5);
    // let y : Option<f64> = Some(5.0);
    let int_origin = Point {
        x : 0,
        y : 0,
    };
    let float_origin = Point {
        x : 0.0,
        y : 0.0,
    };
    let c = Circle {
        x : 0.0,
        y : 0.0,
        radius : 2.0
    };
    print_area(c);
    let d = Square {
        x : 0.0,
        y : 0.0,
        side : 3.0,
    };
    print_area(d);
    print_area(5);
}
