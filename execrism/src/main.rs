struct Point {
    x : i32,
    y : i32,
    z : i32,
}

struct Color(i32, i32, i32);

fn main() {
    println!("Hello, world!");
    let mut origin = Point { x : 0, y : 0, z : 0};
    origin = Point {x : 2, y : 1, ..origin };
    println!("The origin is at ({},{},{})", origin.x, origin.y, origin.z);

    let black = Color(0, 0, 0);
    let point = Point{x : 0, y : 0, z : 0};

    struct Inches(i32);
    let length = Inches(10);
    let Inches(integer_length) = length;
    println!("length is {} inches", integer_length);

    struct Electron;

    enum Message {
        Quit,
        ChangeColor(i32, i32, i32),
        Move {x : i32, y : i32},
        Write(String),
    }

    let x : Message = Message::Move { x : 3, y : 4 };
    enum BoardGameTurn {
        Move {squares : i32},
        Pass,
    }
    let y : BoardGameTurn = BoardGameTurn::Move { squares : 1 };

    let i = 4;
    let number = match i {
        1 | 2 => println!("one or two"),
        3 ... 5 => println!("three through five"),
        _ => println!("something else"),
    };

    let str = '💅';
    let destStr = match str {
        'a' ... 'j' => println!("early letter"),
        'k' ... 'z' => println!("late letter"),
        _ => println!("something else"),
    };

    fn quit() {
        /* ... */
    }

    fn change_color(r : i32, g :i32, b : i32) {
        /* ... */
    }

    fn move_cursor(x :i32, y : i32) {
        /* ... */
    }

    fn process_message(msg : Message) {
        match msg {
            Message::Quit => quit(),
            Message::ChangeColor(r, g, b) => change_color(r, g, b),
            Message::Move {x : x, y : y} => move_cursor(x, y),
            Message::Write(s) => println!("{}", s),
            //_ => println!("{}", "hello"),
        };
    }

    let j = 9;
    let k = match j {
        e @ 1 ... 5 | e @ 8 ... 10 => println!("get a value for range {}", e),
        _ => println!("something else"),
    };

    #[derive(Debug)]
    struct Person {
        name : Option<String>,
    }
    let name = "Steve".to_string();
    let mut x : Option<Person> = Some(Person { name : Some(name) });
    match x {
        Some(Person { name : ref a @ Some(_), ..}) => println!("{:?}", a),
        _ => {}
    }

    enum OptionalInt {
        Value(i32),
        Key(String),
        Missing,
    }

    let miss = OptionalInt::Value(7);
    //let miss = OptionalInt::Key("5".to_string());
    match miss {
        OptionalInt::Value(i) if i > 5  => println!("Got an int bigger than five!"),
        OptionalInt::Value(..) => println!("Got an int!"),
        OptionalInt::Key(s) => println!("Got an String {}", s),
        OptionalInt::Missing => println!("No such luck."),
    }

    let xx = 5;
    match xx {
        ref r => println!("Got a reference to {}", r),
    }

    let mut yy = 5;
    match yy {
        ref mut mr => println!("Got a mutable reference to {}", mr),
    }

    struct PointXY {
        x : i32,
        y : i32,
    }

    let origin = PointXY { x : 5, y : 0};
    match origin {
        PointXY { x : x, y : y, ..} => println!("x is {}, y is {}", x, y),
    }

//    let c = Circle { x : 0.0, y : 0.0, radius : 2.0 };
//    println!("{}", c.area());
//
//    let d = c.grow(2.0).area();
//    println!("{}", d);
    //println!(c.reference());
    //println!(c.mutable_reference());
    //println!(c.takes_ownership());

//    let cc = Circle::new(0.0, 0.0, 4.0);
//    println!("{}", cc.area());
//
//    let dd = CircleBuilder::new().x(3.0).y(4.0).radius(5.0).finalize();
//    println!("{}", dd.area());
//    println!("{}", dd.x);
//    println!("{}", dd.y);
//    println!("{}", dd.radius);
}

#[derive(Clone, Copy, Debug)]
struct A {
    a: f64,
//    b: Vec<String>,
}

struct Circle { // clone  Copy 自己实现
    x : A ,
    y : f64,
    radius : f64,
}

impl Circle {
    fn new(x : A, y : f64, radius : f64) -> Circle {
        Circle {
            x,
            y,
            radius,
        }
    }
    fn area(&self) -> f64 {
        std::f64::consts::PI * (self.radius * self.radius)
    }
    fn grow(&self, increment : f64) -> Circle {
        Circle {x : self.x, y : self.y, radius : self.radius + increment}
    }
    fn reference(&self) {
        println!("taking self by reference!");
    }
    fn mutable_reference(&mut self) {
        println!("taking self by mutable reference");
    }
    fn takes_ownership(self) {
        println!("taking ownership of self");
    }
}

struct CircleBuilder {
    x : A ,
    y : f64,
    radius : f64,
}

impl CircleBuilder {
    fn new() -> CircleBuilder {
        CircleBuilder {
            x : A{a : 0.0},
            y : 0.0,
            radius : 1.0,
        }
    }

    fn x(&mut self, coordinate : A) -> &mut CircleBuilder {
        self.x = coordinate;
        self
    }

    fn y(&mut self, coordinate : f64) -> &mut CircleBuilder {
        self.y = coordinate;
        self
    }

    fn radius(&mut self, radius : f64) -> &mut CircleBuilder {
        self.radius = radius;
        self
    }

    fn finalize(&self) -> Circle {
        Circle {
            x : self.x,
            y : self.y,
            radius : self.radius,
        }
    }
}
