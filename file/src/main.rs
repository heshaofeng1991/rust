use std::fs::File;
use std::io::Write;

fn main() {
    println!("Hello, world!");
    let mut fp = std::fs::File::open("foo.txt").ok().expect("Couldn't open foo.txt");
    let result = fp.write("whatever".as_bytes());
}
